package models.dao;

import models.dto.Category;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryDao {

    public void saveData (Category category) {
        try {
           ConnectionDB connectionDB = new ConnectionDB();
           Statement statement = connectionDB.openConnection();

            // 4- execute query
            String insertQuery = "INSERT INTO tb_category (name) " +
                    "VALUES ("+ "'"+ category.getName() + "'"+ ")";
            statement.executeUpdate(insertQuery);

            // 5- close connection
            connectionDB.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Category> fetchCategories () {
        List<Category> categories = new ArrayList<>();
        try {

            ConnectionDB connectionDB = new ConnectionDB();
            Statement statement = connectionDB.openConnection();
            // 4- execute query
            String insertQuery = "SELECT * FROM tb_category";
            ResultSet resultSet = statement.executeQuery(insertQuery);


            while (resultSet.next()) {
                Category category = new Category();
                category.setId(resultSet.getInt(1));
                category.setName(resultSet.getString(2));

                categories.add(category);
            }

            // 5- close connection
            connectionDB.closeConnection();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }
}
