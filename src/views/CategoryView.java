package views;

import models.dao.CategoryDao;
import models.dto.Category;

import java.util.List;

public class CategoryView {
    public static void main(String[] args) {
        CategoryView categoryView = new CategoryView();
//        categoryView.insertCategory();
        categoryView.fetchCategory();

    }

    void insertCategory () {
        CategoryDao categoryDao = new CategoryDao();
        Category category = new Category();
        category.setName("new Category 123");
        categoryDao.saveData(category);
    }

    void fetchCategory () {
        CategoryDao categoryDao = new CategoryDao();
        List<Category> categoryList = categoryDao.fetchCategories();
        System.out.println(categoryList);
    }
}
